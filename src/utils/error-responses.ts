import { Response } from 'express';

export enum ERROR_TYPE {
  UNKNOWN_ERROR = 'UNKNOWN_ERROR',
  MISSING_FIELD = 'MISSING_FIELD',
  NOT_FOUND = 'NOT_FOUND',
  WRONG_PASSWORD = 'WRONG_PASSWORD',
}

export enum MODEL_TYPE {
  USER = 'User',
}

const handleError = (res: Response, errorType: ERROR_TYPE, model: MODEL_TYPE, param?: any) => {
  const response = formErrorMessage(errorType, model, param);
  return res.status(response.status).send(response.message);
};

const formErrorMessage = (
    errorType: ERROR_TYPE,
    model: MODEL_TYPE,
    param?: any,
  ): { status: number, message: string } => {
    switch(errorType){
    case ERROR_TYPE.UNKNOWN_ERROR:
      return { status: 500, message: `Something went wrong in model ${model}` };
    case ERROR_TYPE.MISSING_FIELD:
      return { status: 400, message: `Fields missing in model ${model}: ${ param.join(', ')}` };
    case ERROR_TYPE.NOT_FOUND:
      return { status: 404, message: `${model} not found` };
    case ERROR_TYPE.WRONG_PASSWORD:
      return { status: 401, message: `Password not match` };
    default:
      return { status: 500, message: `Not handled` };
  }
}

export default handleError;
