import { Request, Response, NextFunction } from 'express';
import _pick from 'lodash/pick';
import _differenceWith from 'lodash/differenceWith';
import _isEqual from 'lodash/isEqual';
import { UserModelProperties } from '../models/user';
import HandleError, { ERROR_TYPE, MODEL_TYPE } from './error-responses';

export enum ROUTE_TYPE {
  USER_CREATE = 'USER_CREATE',
  USER_LOGIN = 'USER_LOGIN',
}

const getRequiredFields = (route: ROUTE_TYPE): { model: MODEL_TYPE.USER, fields: string[] } => {
  switch(route){
    case ROUTE_TYPE.USER_CREATE:
      return {
        model: MODEL_TYPE.USER,
        fields: [ UserModelProperties.email, UserModelProperties.password ],
      };
    case ROUTE_TYPE.USER_LOGIN:
      return {
        model: MODEL_TYPE.USER,
        fields: [ UserModelProperties.email, UserModelProperties.password ],
      };
  }
  return {
    model: MODEL_TYPE.USER,
    fields: [],
  };
}


const checkHasFields = (obj: object, fieldsToCheck: string[]): boolean | string[] => {
  const result = _pick(obj, fieldsToCheck)
  const keys = Object.keys(result)
  if(keys.length !== fieldsToCheck.length){
    return _differenceWith(fieldsToCheck, keys, _isEqual);
  }
  return false;
}

const getValidationMiddleware = (route: ROUTE_TYPE) => (req: Request, res: Response, next: NextFunction ) => {
  const requiredFields = getRequiredFields(route)
  const missingFields = checkHasFields(req.body, requiredFields.fields)
  if(missingFields){
    return HandleError(res, ERROR_TYPE.MISSING_FIELD, requiredFields.model, missingFields);
  }
  return next();
}

export { getValidationMiddleware };
