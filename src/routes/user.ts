import UserController from '../controllers/user';
import { Router } from 'express';
import { getValidationMiddleware, ROUTE_TYPE } from '../utils/middleware-validation';

const router = Router();

router.route('/')
  .post(getValidationMiddleware(ROUTE_TYPE.USER_CREATE), UserController.create)
  .get(UserController.getAll);

export default router;
