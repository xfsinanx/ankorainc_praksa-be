import AuthController from '../controllers/auth';
import { Router } from 'express';
import { getValidationMiddleware, ROUTE_TYPE } from '../utils/middleware-validation';

const router = Router();

router.route('/login')
  .post(getValidationMiddleware(ROUTE_TYPE.USER_LOGIN), AuthController.login);

export default router;
