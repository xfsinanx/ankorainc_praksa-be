import bcrypt from 'bcrypt';
import { Document, model, Schema } from 'mongoose';

export interface IUserModel extends Document {
  email: string;
  password?: string;
  salt?: string;
  authenticate: (password: string) => boolean;
  removeSensitiveData: () => IUserModel;
}

export enum UserModelProperties {
  email = 'email',
  password = 'password',
  salt = 'salt',
}

const UserSchema = new Schema({
  email: {
    required: true,
    type: String,
    unique: true,
  },
  password: {
    required: true,
    type: String,
  },
  salt: {
    type: String,
  },
});

UserSchema.pre<IUserModel>('save', function preSave(next){
  if(this.password && this.isModified('password')) {
    this.salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, this.salt);
  }
  next();
});

UserSchema.methods.authenticate = function authenticate(
  password: string,
) {
  return this.password === bcrypt.hashSync(password, this.salt);
};

UserSchema.methods.removeSensitiveData = function () {
  delete this._doc.salt;
  delete this._doc.password;

  return this._doc;
};

export default model<IUserModel>('User', UserSchema);