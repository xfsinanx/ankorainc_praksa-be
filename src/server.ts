import bodyParser from 'body-parser';
import express, {Request, Response} from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
import userRoutes from './routes/user';
import authRoutes from './routes/auth';

mongoose.connect(
  'mongodb://user123:user123@ds127376.mlab.com:27376/zavrsni-verzija-42',
  {
    useNewUrlParser: true,
  }
)

mongoose.connection.on('error', (err: string) => {
  console.log("Error connecting with db:", err);
});

mongoose.connection.on('connected', () => {
  console.log("Successfully connected with db");
});

const app = express();

app.use(bodyParser.json());

app.use(cors({
  origin: 'http://localhost:3000'
}));

app.get('/', (req, res) => res.send('Hello World!'));

app.get('/get', async(req: Request, res: Response): Promise<Response> => {
  return res.send({
    test: false,
  });
});

app.use('/api/v1/users', userRoutes);
app.use('/api/v1/auth', authRoutes);

app.listen(3500, () => console.log('Example app listening on port 3500!'));