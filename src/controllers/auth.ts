import { Request, Response } from 'express';
import User from '../models/user';
import HandleError, { ERROR_TYPE, MODEL_TYPE } from '../utils/error-responses';

const login = async(req: Request, res: Response): Promise<Response> => {
  try {
    const user = await User.findOne({email: req.body.email});
    if(!user){
      return HandleError(res, ERROR_TYPE.NOT_FOUND, MODEL_TYPE.USER);
    }
    const isPasswordOkay = user.authenticate(req.body.password);
    if(!isPasswordOkay){
      return HandleError(res, ERROR_TYPE.WRONG_PASSWORD, MODEL_TYPE.USER)
    }
    return res.send(user.removeSensitiveData());
  } catch (e) {
    return HandleError(res, ERROR_TYPE.UNKNOWN_ERROR, MODEL_TYPE.USER);
  }
};

export default {
  login,
};
