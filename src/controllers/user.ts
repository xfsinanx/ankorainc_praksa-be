import { Request, Response } from 'express';
import User from '../models/user';
import HandleError, { ERROR_TYPE, MODEL_TYPE } from '../utils/error-responses';

const create = async(req: Request, res: Response): Promise<Response> => {
  try {
    const user = await User.create(req.body);
    return res.send(user.removeSensitiveData());
  } catch (e) {
    return HandleError(res, ERROR_TYPE.UNKNOWN_ERROR, MODEL_TYPE.USER);
  }
};

const getAll = async(_: Request, res: Response): Promise<Response> => {
  try {
    const user = await User.find({}).select('-password -salt');
    return res.send(user);
  } catch (e) {
    return HandleError(res, ERROR_TYPE.UNKNOWN_ERROR, MODEL_TYPE.USER);
  }
}

export default {
  create,
  getAll
};
